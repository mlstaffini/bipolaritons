set term postscript enhanced eps "Times-Roman,28"
set out "boundary.eps"

data1="boundary_GaAs_T=0.340D0_h=0.220d0_to_1.dat"
data2="boundary_GaAs_T=0.340D0_h=0.220d0_to_2.dat"
data3="boundary_GaAs_T=0.340D0_h=0.220d0_to_3.dat"

set xlabel "{/Symbol m} (meV)"
set ylabel "{/Symbol d} (meV) "

plot data1 using 1:2 
     data2 using 1:2
     data3 using 1:2
MODULE widbg_free_energy
  USE global, ONLY: DBG_F, DBG_Psim_F

  IMPLICIT NONE
  INTEGER,PRIVATE, PARAMETER :: DP=KIND(1.0D0)

  ! Free energy for a single mode case, matching the same method as
  ! used in the three mode case, for the purpose of plotting e.g.
  ! n vs mu/T for WIDBG to compare to HFBP and to QMC results
  
CONTAINS
  

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   get_F_parts
  ! VARIABLES:
  !           a,beta - Variational parameters
  !           F0 - Bare grand potential, including all part.  NB
  !                    this is F0 as defined in the notes, including Z
  !           y0     - COfficients of psi0sq
  !           psi0sq - psi_0^2, atomic condensate density
  !           norm   - Total normal density
  ! SYNOPSIS:
  ! Calculate the two parts of grand potential required elsewhere
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE get_F_parts(p_a, p_beta, F0, y0, psi0sq, n)
    USE interpolation, ONLY: get_integral
    USE global, ONLY: U, eX, mu, pi, T, minbeta, alimit

    REAL (KIND=DP), INTENT(IN) :: p_a, p_beta
    REAL (KIND=DP), INTENT(OUT) ::F0, y0, psi0sq, n
    REAL (KIND=DP) :: a, beta, alpha
    REAL (KIND=DP) :: Phi, Z, root0, root1

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Calculate intermediate quantities:
    a=p_a; beta=p_beta


    ! The following should not be required, but for some reason
    ! the nag routine for minimisation insists on calculating
    ! the function oustide the region where it is defined!
    a=p_a; beta=p_beta
    IF (beta .LT. minbeta) beta = minbeta
    
    IF (ABS(a) .GT. alimit) a=alimit*a/ABS(a)

    alpha=a*beta

    ! Temporary quantities appearing below: mode energies at upper and
    ! lower limits of integral
    root0=sqrt((beta+alpha)*(beta-alpha))
    root1=eX*sqrt((1.0D0+(beta+alpha)/eX)*(1.0D0+(beta-alpha)/eX))

    ! These are the n_a, n_b, Phi_a, Phi_b, Z_a, Z_b terms
    IF (beta .EQ. 0.0D0)  THEN
       n=0.0D0
       Phi=0.0d0
       Z=0.0D0
    ELSE
       n=(alpha**2)*(eX+root1-root0)&
            & /((beta+root0)*(eX+beta+ root1)*(4*pi))
       Phi=alpha*log( ( eX+beta+root1 )/(beta+root0) )/(4*pi)
       Z=0.5D0*alpha*Phi-beta*n + &
            & (alpha**4) * (1.0D0+(root1-root0)/eX) &
            &            * (1.0D0+(2*beta+root1+root0)/eX) / &
            & (16*pi*((beta+root0)*(1.0D0+(beta+ root1)/eX))**2)
    end IF
    
    ! No change for zero T case.
    IF (T .GT. 0.0D0) THEN
       ! Finite T part of n is analytic
       
       n=n+ (T/(2*pi)) * Log((1-exp(-root1/T))/(1-exp(-root0/T)))
       
       ! Add the finite temperature parts to Phi and Z-TS
       
       Phi = Phi + get_integral(a, beta, "FP")
       Z = Z + get_integral(a, beta, "ZP")
    end IF

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Calculate final quantities.  
    F0= Z - mu*N  + 0.5*U*(2*n**2 + Phi**2) 


    ! Intermediate quantities for finding psim and psi0sq (atom
    ! condensate density)
    y0 = 0.5*mu  - 0.5*U*(2*n + Phi)

    IF (y0 .GT. 0.0D0) THEN
       psi0sq = 2*y0/U
    ELSE
       psi0sq = 0.0d0
    end IF
    
  END SUBROUTINE get_F_parts
  
  



  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   calculate_F
  ! VARIABLES:
  !           a, beta - Variational parameters for a,b,m
  ! SYNOPSIS:
  ! Calculate the grand potential at the given parameters
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION calculate_F(a,beta)
    USE global, ONLY: U, V,g,W,nu, mu

    REAL (KIND=DP), INTENT(IN) ::a,beta
    REAL (KIND=DP) :: calculate_F
    REAL (KIND=DP) :: F0,  psi0sq,y0, norm

    CALL get_F_parts(a, beta, F0, y0, psi0sq, norm)

    calculate_F = F0 - 2*y0*psi0sq + 0.5*U*psi0sq**2 
    

    IF (DBG_F) WRITE(99,'(2(3(D15.8,2X),3X),(5(D15.8,2X)))') &
         &  a, beta, calculate_F, F0, psi0sq

  END FUNCTION calculate_F


  SUBROUTINE get_F_2D(n,xc,fc,iuser,ruser)
    INTEGER, INTENT(IN) :: n
    REAL (KIND=DP), INTENT(IN) :: xc(n)
    REAL (KIND=DP), INTENT(OUT) :: fc
    INTEGER, INTENT(INOUT) :: iuser(*)
    REAL (KIND=DP), INTENT(INOUT) :: ruser(*)
    REAL (KIND=DP) :: dummy

    dummy=ruser(1); dummy=1.0D0*iuser(1)
    fc=calculate_F(xc(1), xc(2))

  END SUBROUTINE get_F_2D


end MODULE widbg_free_energy

MODULE widbg_minimisation
  USE global, ONLY: DBG_F

  IMPLICIT NONE
  INTEGER,PRIVATE, PARAMETER :: DP=KIND(1.0D0)


  ! This module contains the code for minimisation; both the minimisatoin
  ! of the separate grand potential forms, and the comparison between
  ! the two free energies
  
CONTAINS
  
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   find_minimum
  ! VARIABLES:
  !           a, beta - Parameters of variational wavefunction
  !           F       - Grand Potential of minimum found
  !           psi0sq  - Atomic condensate density
  !           psim    - Molecular condensate
  !           rho     - Density
  ! SYNOPSIS:
  ! Find the mininimum grand potential state for the current parameters
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE find_minimum(a,beta,F,psi0sq,rho)
    USE widbg_free_energy, ONLY: get_f_2d, calculate_F, get_f_parts
    USE global, ONLY:  alimit, minbeta
    USE utility, ONLY: interpolate
    USE nag_library, ONLY: e04jyf, e04aba

    ! Maximum for range of beta
    REAL (KIND=DP) :: maxbeta=1.0D4
    ! Upper value for starting guesses of beta
    REAL (KIND=DP) :: maxbeta0=5.0D2
    
    REAL (KIND=DP), INTENT(OUT) ::a,beta,F,psi0sq, rho
    REAL (KIND=DP) :: F0, norm, y0

    ! Lengths of work arrays required for E04JYF
    INTEGER, PARAMETER :: liw=8
    INTEGER, PARAMETER :: lw=87
    INTEGER, PARAMETER :: Na=10, Nb=10, attempts=801

    LOGICAL :: acceptable=.TRUE.
    INTEGER :: ia, ib, attempt
    INTEGER :: ibound, ifail, iuser(1), iw(liw), i_min(1)
    REAL (KIND=DP) :: a_trial, beta_trial
    REAL (KIND=DP) :: bl(2), bu(2), ruser(2), w(lw), x(2)

    REAL (KIND=DP) :: f_m(attempts), x_m(attempts,2)

    attempt=0
    CALL record_attempt(0.0D0, minbeta)

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Two dimensional minimisation
    DO ia=1, Na
       DO ib=1, Nb
          a_trial=INTERPOLATE(-alimit**2, alimit**2, ia, Na,.FALSE.)
          beta_trial =INTERPOLATE(0.1D0, maxbeta0, ib, Nb,.TRUE.)
             

          x(1) = a_trial; x(2) = beta_trial

          ! Set up the bounds (gets reset by e04jyf)
          ibound=0
          bl = (/ -alimit, minbeta /)
          bu = (/  alimit, maxbeta /)

          ifail= +1
          
          CALL e04jyf(2, ibound, get_f_2d, bl, bu, x, &
               & f, iw, liw, w, lw, iuser, ruser, ifail)
          CALL check_error(ifail,acceptable)
          IF (acceptable) THEN
             IF(DBG_F) WRITE(99,*) "# Completed 2D"

             CALL record_attempt(x(1),x(2))
          ELSE
             IF(DBG_F) WRITE(99,*) "# Failed 2D"
          end IF

       end DO
    end DO
    
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Determine which of the various attempts was overall best
    i_min=MINLOC(f_m(1:attempt))   
    f=f_m(i_min(1)); a=x_m(i_min(1),1); beta=x_m(i_min(1),2)

    ! Find psi0sq, psim, and make sure it is positive
    CALL get_f_parts(a,beta, F0, y0, psi0sq, norm)
    psi0sq=MAX(psi0sq, 0.0D0)

    ! Total density
    rho = psi0sq + norm

    
  CONTAINS

    SUBROUTINE check_error(ifail, acceptable)
      INTEGER :: ifail
      LOGICAL :: acceptable

      SELECT CASE(IFAIL)
      CASE(1)
         WRITE(*,*) "Invalid input to minimum finding routine"
         acceptable=.FALSE.
         STOP
      CASE(2)
         acceptable=.FALSE.
      CASE DEFAULT
         acceptable=.TRUE.
      end SELECT
      
    end SUBROUTINE check_error
    

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! NAME:   record_attempt
    ! SYNOPSIS:
    ! Record the current values of a, beta in the list of possible
    ! minima.  (Find out the value of F at this point, and record
    ! it too)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    SUBROUTINE record_attempt(a,beta)
      REAL (KIND=DP), INTENT(IN) :: a, beta

      ! Increment counter
      attempt=attempt+1
      IF (attempt .GT. SIZE(f_m)) THEN
         WRITE(*,*) "Overran attempt record"
         STOP
      end IF

      f_m(attempt)=calculate_f(a,beta)
      IF(DBG_F) WRITE(99,*)

      x_m(attempt,1) = a
      x_m(attempt,2) = beta

    END SUBROUTINE record_attempt

  END SUBROUTINE find_minimum

END MODULE widbg_minimisation


PROGRAM phase_diagram
  USE global, ONLY: allow_sigma, proc, nu, g, dbg_F
  USE utility, ONLY: interpolate, parse_logical,pi
  USE minimisation, ONLY: find_minimum, get_soln_type
  USE free_energies, ONLY: get_f_parts, get_mf_f_parts
  use ex_ph_params, ONLY: read_physical_params, read_interpolation_params, &
       & set_model_params, set_param, write_params_header, density_factor
  USE interpolation, ONLY: use_interpolation

  IMPLICIT NONE
  INTEGER, PARAMETER :: DP=KIND(1.0D0)

  ! Results of optimisation
  REAL (KIND=DP) ::  a(3), beta(3), xi, chi, F, rho, psi0sq, psim
  REAL (KIND=DP) :: la, lb
  REAL (KIND=DP) :: lamin, lamax, lbmin, lbmax
  INTEGER :: ila, ilb, Nla, Nlb
  INTEGER :: FILE, OUT_FULL=27, OUT_MFT=28, typev
  CHARACTER :: prefix*64, filename*128, lastr*2, lbstr*2, allow_sigma_str, &
       & interpstr*1

  ! GLOBAL SWITCHES
  DBG_F=.FALSE.

  ! This program plots a phase diagram vs chemical potential and
  ! exciton-photon detuning, working in physical units (no rescaling).

  ! It takes account of how the model parameters vary as the exciton
  ! photon detuning changes.

  CALL read_physical_params()
  WRITE(*,*) "Allow different values of variational params alpha, beta?"
  READ(*,*) allow_sigma_str; allow_sigma=(SCAN(allow_sigma_str,'yYtT') .GT. 0) 
  CALL read_interpolation_params()
  WRITE(*,*) "Outer loop: MIn, Max, Number, What"
  READ(*,*) lamin, lamax, Nla, lastr
  WRITE(*,*) "Inner loop: MIn, Max, Number, What"
  READ(*,*) lbmin, lbmax, Nlb, lbstr
  WRITE(*,*) "Filename prefix, proc, Use interpolation?"
  READ(*,*) prefix, proc, interpstr
  use_interpolation=parse_logical(interpstr)

  filename = TRIM(ADJUSTL(prefix))//".dat"
  open(unit=OUT_FULL, file=filename, status='replace')
  filename = TRIM(ADJUSTL(prefix))//"_mft.dat"
  open(unit=OUT_MFT, file=filename, status='replace')

  DO FILE=OUT_FULL, OUT_MFT
     CALL write_params_header(FILE)
  end DO
  IF (DBG_F) THEN
     CALL write_params_header(99)
  end IF
  

  WRITE(OUT_FULL, '(X,3(A12,X),A1,X,13(A12,X))') lastr, lbstr, "F", &
       "T","a", "a","a", "beta", "beta", "beta","xi","chi", "psi0sq","psim", "rho", "nu", "Width"
  WRITE(OUT_MFT, '(X,3(A12,X),A1,X,5(A12,X))') lastr, lbstr, "F", &
       "T","psi0sq","psim", "rho", "nu", "Width"

  DO ila=1, Nla
     la=INTERPOLATE(lamin, lamax, ila,Nla,.FALSE.)
     CALL set_param(lastr, la)

     DO ilb=1, Nlb
        lb=INTERPOLATE(lbmin, lbmax, ilb,Nlb,.FALSE.)
        CALL set_param(lbstr, lb)
        
        ! Convert the current physical paramters to model parameters
        CALL set_model_params()

        ! Locate extrema...
        CALL find_minimum(a, beta, xi, chi, F, psi0sq, psim, rho)
        rho = rho * density_factor
        typev=get_soln_type(a, beta, psi0sq, psim)
           
        WRITE(OUT_FULL,'(X,3(D12.5,X),I1,X,13(E13.5E3,X))') la, lb, f, &
             & typev, a, beta, xi, chi, psi0sq, psim, rho, nu,g*g

        CALL flush(OUT_FULL)

        ! Mean field theory, for comparison
        CALL get_mf_f_parts(chi,F, psi0sq, psim)
        typev=get_soln_type((/0.0D0, 0.0D0, 0.0D0/),&
             &              (/0.0D0, 0.0D0, 0.0D0/), psi0sq, psim)
        WRITE(OUT_MFT,'(X,3(D12.5,X),I1,X,7(E13.5E3,X))') la, lb, f, &
             & typev, psi0sq, psim, rho, nu,g*g


        CALL flush(OUT_MFT)

     end DO

     WRITE(OUT_FULL,*); WRITE(OUT_MFT,*)
  end DO
  CLOSE(OUT_FULL); CLOSE(OUT_MFT)
  
END PROGRAM phase_diagram


MODULE global
  IMPLICIT NONE
  INTEGER,PRIVATE, PARAMETER :: DP=KIND(1.0D0)

  ! For comparison to other notation
  ! U/2 = chi_11 = chi_22
  ! V   = V_bg = chi_12
  ! W   = chi_33     
  ! g  is inter-channel coupling
  ! nu is detuning of molecule
  ! eX is cutoff energy scale.
  REAL (KIND=DP) :: U, V, W, g, nu, mu, eX, h
  ! T is temperature (units of energy)
  ! mmass is molecule (biexciton) mass in polariton mass units
  REAL (KIND=DP) :: T, mmass
  REAL (KIND=DP), PARAMETER ::  pi=3.14159265358979d0
  INTEGER :: proc

  ! Do not allow a to go to 1 as it diverges in 2D at T>0
  ! Do not allow beta to go to 0, as above,
  REAL (KIND=DP) :: alimit=0.99999, minbeta=1.0D-12
  

  ! Switch controlling with to print out values of grand potential and
  ! free energy (finite T version)
  LOGICAL :: DBG_Omega=.FALSE., DBG_Psim_Omega=.FALSE., DBG_RANGE=.FALSE.
  LOGICAL :: DBG_F=.FALSE., DBG_Psim_F=.FALSE.
  ! Switch controlling whether we are considering full
  ! optimisation (allowing distinct alpha, beta), or the simpler
  ! case
  LOGICAL :: ALLOW_SIGMA=.TRUE.

  ! Should cutoff affect finite temperature parts of integral, if
  ! false, cutoff only appears in T=0 parts.
  LOGICAL :: finite_T_cutoff=.FALSE.
  
END MODULE global

PROGRAM phase_diagram
  USE global, ONLY: eX, U, mu, proc, T
  USE interpolation, ONLY: setup_interpolation_widbg, test_interpolation
  USE utility, ONLY: interpolate
  USE widbg_minimisation, ONLY: find_minimum
  USE free_energies, ONLY: get_f_parts

  IMPLICIT NONE
  INTEGER, PARAMETER :: DP=KIND(1.0D0)

  ! Results of optimisation
  REAL (KIND=DP) :: a, beta, F, rho, psi0sq
  REAL (KIND=DP) :: la, lb
  REAL (KIND=DP) :: lamin, lamax, lbmin, lbmax
  INTEGER :: ila, ilb, Nla, Nlb
  INTEGER :: na_int, nbeta_int
  REAL (KIND=DP) :: maxbeta_int
  INTEGER ::  OUT_FULL=27, typev
  CHARACTER :: prefix*64, filename*128, lastr*2, lbstr*2
  LOGICAL :: reset_int=.TRUE. ! Do we need new interpolation? Initially yes.

  ! This program prints things out vs T and mu most likely
  
  WRITE(*,*) "eX, U, mu, T"
  READ(*,*) eX, U, mu, T
  WRITE(*,*) "Number of inteprolation points for a, beta and max beta"
  READ(*,*) na_int, nbeta_int, maxbeta_int
  WRITE(*,*) "Outer loop: MIn, Max, Number, What"
  READ(*,*) lamin, lamax, Nla, lastr
  WRITE(*,*) "Inner loop: MIn, Max, Number, What"
  READ(*,*) lbmin, lbmax, Nlb, lbstr
  WRITE(*,*) "Filename prefix, proc"
  READ(*,*) prefix, proc
  filename = TRIM(ADJUSTL(prefix))//".dat"
  open(unit=OUT_FULL, file=filename, status='replace')

  WRITE(OUT_FULL, '(X,3(A12,X),A1,X,7(A12,X))') lastr, lbstr, "F", &
       "T","a", "beta", "psi0sq", "rho"

  DO ila=1, Nla
     la=INTERPOLATE(lamin, lamax, ila,Nla,.FALSE.)
     CALL set_param(lastr, la)

     DO ilb=1, Nlb
        lb=INTERPOLATE(lbmin, lbmax, ilb,Nlb,.FALSE.)
        CALL set_param(lbstr, lb)
        
        IF (reset_int) THEN
           ! Set up interpolation if required, and mark as clean
           CALL setup_interpolation_widbg(maxbeta_int, nbeta_int, na_int)
           !CALL test_interpolation("FB", maxbeta_int/10)

           reset_int=.FALSE.
        end IF


        ! Locate extrema...
        CALL find_minimum(a, beta, F, psi0sq, rho)
        IF (psi0sq .GT. 0.0D0) THEN
           typev=2
        else
           typev=0
        end IF        
           
        WRITE(OUT_FULL,'(X,3(D12.5,X),I1,X,4(D12.5,X))') la, lb, f, &
             & typev, a, beta, psi0sq, rho

        CALL flush(OUT_FULL)



     end DO

     WRITE(OUT_FULL,*)
  end DO
  CLOSE(OUT_FULL)
CONTAINS

  SUBROUTINE set_param(str, val)
    CHARACTER , INTENT(IN) :: str*2
    REAL (KIND=DP), INTENT(IN) :: val

    ! Determine whether we need to reset interpolation,
    ! this is required if T, or cutoff changes.  Cutoff
    ! depends on mass ratio thus detuning

    SELECT CASE(str)
    CASE("U0")
       U=val
    CASE("MU")
       mu=val
    CASE("TE")
       T=val; reset_int=.TRUE.
    end SELECT

  end SUBROUTINE set_param
  
end PROGRAM phase_diagram

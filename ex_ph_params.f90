MODULE ex_ph_params
  IMPLICIT NONE
  INTEGER,PRIVATE, PARAMETER :: DP=KIND(1.0D0)


  ! File to cope with physical exciton-photon parameters - contains 
  ! the physical parameters, and routine to update the model accordingly


  ! Bare parameters: U0, V0 and W0 are interactions scaled by exciton
  ! mass, so are of order 1.  Delta0 is the bare resonance width, this
  ! is in meV, and is comparable to the biexcitonic binding energy,
  ! (ebind_xx). The exciton binding energy is ebind_x, used in setting
  ! the cutoff scale.  delta_exph is the exciton photon detuning.
  ! OmegaR is Rabi splitting
  REAL (KIND=DP) :: U0, V0, W0, Delta0, Rabi, delta_exph, &
       & ebind_x, ebind_xx, bare_mass_ratio, mex

  ! Do we need new interpolation? Initially yes;  set to false after
  ! each interpolation, reset to true if something important changes
  LOGICAL :: reset_int=.TRUE. 

  ! Interpolation parameters
  REAL (KIND=DP) :: maxbeta_int
  INTEGER :: na_int, nbeta_int
 
  ! Factor to convert density into inverse square microns
  REAL (KIND=DP), PARAMETER :: hbar2onme=7.62D-5
  REAL (KIND=DP) ::  density_factor
  
CONTAINS
  

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   read_physical_params
  ! SYNOPSIS:
  ! Physical parameters of polariton problem
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE read_physical_params()
    USE global, ONLY: mu, T, h
   
    WRITE(*,*) "Static parameters"
    WRITE(*,*) "Dimensionless interactions (excitonic scale):"
    WRITE(*,*) "U0, V0, W0, Mph/Mex, Mex/Me, Zeeman splitting (meV)"
    READ(*,*) U0, V0, W0, bare_mass_ratio, mex,h 
    WRITE(*,*) "Energies:  Delta0, Rabi, Ebind_x, Ebind_xx, delta_exph, mu, T"
    READ(*,*) Delta0, Rabi, ebind_x, ebind_xx, delta_exph, mu, T
 
  END SUBROUTINE read_physical_params
  

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   read_interpolation_params
  ! SYNOPSIS:
  ! Parameters for interpolation 
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE read_interpolation_params()
    
    WRITE(*,*) "Number of inteprolation points for a, beta and max beta"
    READ(*,*) na_int, nbeta_int, maxbeta_int

  END SUBROUTINE read_interpolation_params



  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   write_params_header
  ! VARIABLES:
  !           FILE - To write to
  ! SYNOPSIS:
  ! Write parameter header to file
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE write_params_header(FILE)
    USE global, ONLY: mu, nu, g, T, h
    INTEGER :: FILE
    
    WRITE(FILE, '("# Bare vals   ",4(A7," =",F10.5,X))') &
          & "U0",U0,"V0",V0,"W0",W0, "mph/mex", bare_mass_ratio
    WRITE(FILE, '("# Bare vals   ",5(A7," =",F10.5,X))') &
          & "mex/m0", mex, "D0", Delta0, "Rabi", Rabi, &
          &  "Ebind BX",ebind_xx, "Ebind X", ebind_x 
    WRITE(FILE, '("# Bare vals   ",4(A7," =",F10.5,X))') &
          & "Delta XP", delta_exph, "mu", mu, "T", T
    WRITE(FILE, '("# Derived     ",4(A7," =",F10.5,X))') &
          & "g", g, "nu", nu
    WRITE(FILE, '("# Field     ",4(A7," =",F10.5,X))') &
          & "h", h

  END SUBROUTINE write_params_header


  SUBROUTINE set_model_params()
    USE interpolation, ONLY: setup_interpolation, test_interpolation
    USE global, ONLY: U,V,W,g,nu, eX, mmass, h

    ! Intermediate quantities used in converting physical properties
    ! to final quantities.  
    REAL (KIND=DP) :: hopfield_ex, hopfield_ph, mass_ratio, splitting

    splitting = sqrt(delta_exph**2 + Rabi**2)

    ! Hopfield coefficients of LP
    hopfield_ex = sqrt(0.5D0* (1.0D0 + delta_exph/splitting))
    hopfield_ph = sqrt(0.5D0* (1.0D0 - delta_exph/splitting))

    ! LP mass over exciton mass.  (bare ratio is photon over exciton)
    mass_ratio  = 2.0D0 *bare_mass_ratio/ &
         & ( bare_mass_ratio*(1.0D0 + delta_exph/splitting) &
         &                  +(1.0D0 - delta_exph/splitting))
    
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Interactions assumed excitonic, so scale with mass ratio and Hopfield.
    U = U0 * mass_ratio * hopfield_ex**4
    V = V0 * mass_ratio * hopfield_ex**4
    W = W0 * mass_ratio * hopfield_ex**4

    ! Currently, zeeman splitting read in directly form intput
    !h=0.0d0

    ! Delta is also excitonic, but note that we use g, i.e. coupling,
    ! where g^2 is the resonance width (Delta), and so:
    g = sqrt(Delta0 * mass_ratio) * hopfield_ex**2

    ! Detuning set by LP energy and bare binding energy.  Note that 
    ! binding energy is given as a positive value.  Note that this
    ! is energy of bound biexciton - 2*LP energy.
    nu = - ebind_xx - delta_exph + splitting
    
    ! Cutoff energy, set from exciton energy, rescalled by mass
    ! ratio (i.e. assuming cutoff length is Bohr radius
    eX = ebind_x / mass_ratio

    ! Biexciton mass over LP mass, FIXME, which to use?
    !mmass = 2.0 
    mmass = 2.0 / mass_ratio
    
    ! Factor to multiply dimensionless density by in order to
    ! get units of inverse area.  Bare values are in energy units,
    ! tilde{rho} = rho * hbar^2/m_pol.  
    ! To rescale, mass_ration = m_pol/m_ex, mex=m_ex/m_e, 
    ! and denominator puts things in area units.
    density_factor = mass_ratio * mex / hbar2onme

    IF (reset_int) THEN
       ! Set up interpolation if required, and mark as clean
       CALL setup_interpolation(maxbeta_int, nbeta_int, na_int)
       !CALL test_interpolation("ZP", maxbeta_int/10)
       reset_int=.FALSE.
    end IF

  end SUBROUTINE set_model_params
  


  SUBROUTINE set_param(str, val)
    USE global, ONLY:  mu, T

    CHARACTER , INTENT(IN) :: str*2
    REAL (KIND=DP), INTENT(IN) :: val

    ! Determine whether we need to reset interpolation,
    ! this is required if T, or cutoff changes.  Cutoff
    ! depends on mass ratio thus detuning

    SELECT CASE(str)
    CASE("U0")
       U0=val
    CASE("V0")
       V0=val
    CASE("W0")
       W0=val
    CASE("D0")
       Delta0=val
    CASE("EB")
       ebind_xx=val; reset_int=.TRUE.
    CASE("WR")
       rabi=val; reset_int=.TRUE.
    CASE("XP")
       delta_exph=val; reset_int=.TRUE.
    CASE("MU")
       mu=val
    CASE("TE")
       T=val; reset_int=.TRUE.
    end SELECT
  end SUBROUTINE set_param


END MODULE ex_ph_params

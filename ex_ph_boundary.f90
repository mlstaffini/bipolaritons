PROGRAM phase_diagram
  USE global, ONLY: allow_sigma, proc, nu, g, pi
  USE utility, ONLY: interpolate, assert, parse_logical
  use ex_ph_params, ONLY: read_physical_params, read_interpolation_params, &
       & set_model_params, set_param, write_params_header
  USE interpolation, ONLY: use_interpolation
  

  IMPLICIT NONE
  INTEGER, PARAMETER :: DP=KIND(1.0D0)

  LOGICAL :: DBG=.TRUE., MFT=.FALSE.

  ! Results of optimisation
  REAL (KIND=DP) ::  a(3), beta(3),xi,chi, F, rhol, rho, psi0sq, psim
  REAL (KIND=DP) :: psi0sql,psiml,xil,chil
  REAL (KIND=DP) :: la
  REAL (KIND=DP) :: lamin, lamax, lbmin, lbmax
  REAL (KIND=DP) :: ranges(4,4)
  INTEGER :: ila, ilb, iter, Nla, Nlb, Nlbiter, nranges
  INTEGER, PARAMETER :: OUT(3)=(/27,28,29/)
  INTEGER :: typevl, typevr, typevc, typevo, typevn
  REAL (KIND=DP) :: lbl, lbr, lbc, lbo, lbn, mun
  CHARACTER :: prefix*64, filename*128, lastr*2, lbstr*2, allow_sigma_str
  CHARACTER :: valstr, mftstr*1, interpstr*1

  ! This program finds the phase boundary by using a rough grid, and
  ! then zooming in on where the type varies.

  ! It takes account of how the model parameters vary as the exciton
  ! photon detuning changes.

  CALL read_physical_params()
  WRITE(*,*) "Allow different values of variational params alpha, beta?"
  READ(*,*) allow_sigma_str; allow_sigma=(SCAN(allow_sigma_str,'yYtT') .GT. 0) 
  CALL read_interpolation_params()
  WRITE(*,*) "Outer loop: Min, Max, Number, What"
  READ(*,*) lamin, lamax, Nla, lastr
  WRITE(*,*) "Search range: MIn, Max, Grid number, Bisection number, What"
  READ(*,*) lbmin, lbmax, Nlb, Nlbiter, lbstr
  WRITE(*,*) "Filename prefix, MFT?, proc, interpolation"
  READ(*,*) prefix, mftstr, proc, interpstr
  mft=parse_logical(mftstr)
  use_interpolation=parse_logical(interpstr)
  
  ! Open files:
  DO iter=1,3
     WRITE(valstr,'(I1)') iter
     filename = TRIM(ADJUSTL(prefix))//"_to_"//valstr//".dat"
   
     open(unit=OUT(iter), file=filename, status='replace')
     
     WRITE (*,*) iter, filename, out(iter)
     
     CALL write_params_header(OUT(iter))
     WRITE(OUT(iter), '("#",3(A12,X),A1,X,19(A13,X))') lastr, lbstr, "F",  "T",&
          & "a", "a","a", "beta", "beta", "beta", "xi left","xi right", "chi left", &
          & "chi right", "psi0sq left","psi0sq right","psim left","psim right",&
          & "mu left","rho Left","rho Right", "nu", "Width"
  end DO

  DO ila=1, Nla
     la=INTERPOLATE(lamin, lamax, ila,Nla,.FALSE.)
     CALL set_param(lastr, la)

     IF (DBG) WRITE(*,*)
     IF (DBG) WRITE(*,*) "New range", la

     ! Get the grid
     lbo=INTERPOLATE(lbmin, lbmax, 1, Nlb,.FALSE.)
     CALL wrap_minim(lbo, typevo)
     lbloop: DO ilb=2, Nlb
        lbn=INTERPOLATE(lbmin, lbmax, ilb,Nlb,.FALSE.)
        CALL wrap_minim(lbn, typevn)

        ! Reset number of ranges
        nranges=0
        
        IF (typevn .NE. typevo) THEN
           ! Record left/right positions, and old position for record
           CALL push_interval(lbo, lbn, typevo, typevn)
        end IF
        ! Record last position
        lbo=lbn; typevo=typevn

        ! While there are points to find,  Divide and concur
        DO WHILE (nranges .GT. 0)
           IF (DBG) WRITE(*,*) "Refining range #", nranges
           CALL pop_interval(lbl, lbr, typevl, typevr)

           DO iter=1, Nlbiter-1
              lbc=0.5*(lbl+lbr); CALL wrap_minim(lbc, typevc)
              
              IF (typevc.EQ.typevl) THEN
                 typevl=typevc; lbl=lbc
              ELSE IF (typevc.EQ.typevr) THEN
                 typevr=typevc; lbr=lbc
              ELSE
                 IF (DBG) WRITE(*,*) "New type found iteration"
 
                 ! Add new range to search to right:
                 CALL push_interval(lbc, lbr, typevc, typevr)

                 ! Change current search to search to left
                 typevr=typevc; lbr=lbc
              end IF
           end DO
           ! Record; First call to left, and record "Old" density, 
           ! then switch to right.
           lbc=lbl; CALL wrap_minim(lbc, typevc); rhol=rho
           psi0sql=psi0sq; psiml=psim;mun=lbc; xil=xi;chil=chi
           lbc=lbr; CALL wrap_minim(lbc, typevc)
           CALL write_cur_point(typevc)

           IF (DBG) WRITE(*,*)  "Completed refinement"
        end DO
        
     end DO lbloop

  end DO

  DO iter=1,3
     CLOSE(OUT(iter))
  end DO

CONTAINS 


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   wrap_minim(val, typev)
  ! VARIABLES:
  !           val   - Value of b param
  !           typev - type
  ! SYNOPSIS:
  ! Wrapper to do minimisation and return type
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE wrap_minim(val, typev)
    USE minimisation, ONLY: find_minimum, get_soln_type
    USE free_energies, ONLY:  get_mf_f_parts
    USE ex_ph_params, ONLY: density_factor

    REAL (KIND=DP), INTENT(IN) :: val
    INTEGER, INTENT(OUT) :: typev

    CALL set_param(lbstr, val)

    ! Convert the current physical parameters to model parameters
    CALL set_model_params()

    ! Locate extrema;  mf case is trivial minimisation
    IF (MFT) THEN
       CALL get_mf_f_parts(chi, F, psi0sq, psim)
       a=(/0.0D0, 0.0D0, 0.0D0/)
       beta=(/0.0D0, 0.0D0, 0.0D0/)
       rho = 2.0d0*psi0sq  + 2.0d0*psim**2 
    ELSE
       CALL find_minimum(a, beta, xi, chi, F, psi0sq, psim, rho)
    end IF
    
    ! Convert the density to physical density
    rho = rho * density_factor

    typev=get_soln_type(a, beta, psi0sq, psim)

    IF (DBG) WRITE(*,*) val, psim, psi0sq, typev
    
  END SUBROUTINE wrap_minim
  
  

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   write_cur_point
  ! VARIABLES:
  !           typev
  ! SYNOPSIS:
  ! Write current paramter point to appropriate file
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE write_cur_point(typev)
    INTEGER, INTENT(IN) ::typev

    IF ((typev .GE. 1 ) .AND. (typev .LE. 3)) THEN
       WRITE(OUT(typev),'(X,3(D12.5,X),I1,X,19(E13.5E3,X))') la, lbc, f, &
            & typev, a, beta,xil,xi,chil, chi, psi0sql,psi0sq, psiml,psim, &
            & mun,rhol, rho, nu,g*g
       CALL flush(OUT(typev))
    ELSE
       WRITE(*,*) "Tried to record switch to type: ", typev
    end IF
    
    
    
  END SUBROUTINE write_cur_point


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   pop_interval/push_interval
  ! VARIABLES:
  !           xl, xr - Coordinates
  !           vl, vr - Values
  ! SYNOPSIS:
  ! Pop the right most value
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE pop_interval(xl,xr,vl,vr)
    REAL (KIND=DP), INTENT(OUT) ::xl,xr
    INTEGER, INTENT(OUT) ::vl,vr

    CALL assert(nranges .GT. 0, "Trying to pop empty interval stack")

    xl=ranges(nranges,1)
    xr=ranges(nranges,2)
    vl=FLOOR(ranges(nranges,3))
    vr=FLOOR(ranges(nranges,4))
    
    nranges=nranges-1

  END SUBROUTINE pop_interval

  SUBROUTINE push_interval(xl,xr,vl,vr)
    REAL (KIND=DP), INTENT(IN) ::xl,xr
    INTEGER, INTENT(IN) ::vl,vr

    nranges=nranges+1

    CALL assert(nranges .LT. size(ranges,1), "Too many subranges")
    ranges(nranges,:) = (/ xl, xr, 1.0D0*vl, 1.0D0*vr /)
    
  END SUBROUTINE push_interval


END PROGRAM phase_diagram


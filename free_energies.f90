MODULE free_energies
    USE global, ONLY: DBG_F, DBG_Psim_F, DBG_RANGE
    USE utility, ONLY: Pi

!!! Global contains logical switches + variable def; the module only uses a few DBG !!!

    IMPLICIT NONE
    INTEGER, PRIVATE, PARAMETER::DP=KIND(1.0D0)
!!! Private sets things to be available only within module. Parameter is to assign values ? !!!

    ! This module contains the code to find the grand potential, and the
    ! associated parameters involved, for both the ASF and MSF phases.

!!! Selects the 'boundary' values where a=+-b, and all possible combinations of minus and plus - pr something like that anyway. & is the continuation character. !!!
    REAL (KIND=DP), DIMENSION(3,4) :: &
        & afac =reshape((/ 1.0D0,-1.0D0, 1.0D0, &
        &                  1.0D0,+1.0D0, 1.0D0, &
        &                  1.0D0, 0.1D0, 1.0D0, &
        &                  0.1D0, 1.0D0, 1.0D0 /), (/3,4/)), &
!!! Reshaping function turns the 12x1 array into a 3x4 array !!!
        & bfac =reshape((/ 1.0D0,+1.0D0, 1.0D0, &
        &                  1.0D0, 1.0D0, 1.0D0, &
        &                  1.0D0, 0.1D0, 1.0D0, &
        &                  0.1D0, 1.0D0, 1.0D0 /), (/3,4/))

    CONTAINS
!!! Separates body of a main program, external subprogram, or module subprogram from any internal subprograms that it may contain. Similarly, it separates the specification part of a module from any module subprograms. !!!


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! NAME:   find_optimal_psi
! VARIABLES:
!           y0, yu, yd, yud  - Values to set in module
!           psi0sq, psim - Values to return
! SYNOPSIS:
! Get the optimal values of psi0 and psim.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

SUBROUTINE find_optimal_psi(yu,yd,yud,ym,msrc,chi,psi0sq,psim)
!!! If a subroutine does receive arguments, then you must use parentheses in its declaration. If there is more than one argument, you must separate the arguments with commas. !!!
    USE nag_library, ONLY: c02AKF
!!! NAG library chapters are supplied like this, you call chapter and routine within !!!

    USE global, ONLY: U, V, W, g, nu, mu, proc

    REAL (KIND=DP), INTENT(IN) :: yu, yd, yud, ym, msrc, chi
    REAL (KIND=DP), INTENT(OUT) ::  psi0sq, psim
    REAL (KIND=DP) :: geff, UplusVeff, yeff
!!! INTENT specified variables as input, output or both. Here out are psi's to pass through outer minimisation !!!
!!! Defining y's here, msrc is coeff of the linear term w.r.t. psi_m with difference in phi's and the sin cos !!!

    REAL (KIND=DP) :: f(7), x(7)
    REAL (KIND=DP) :: coeffl(5), coeffr(5), xc, dcoeff(4)
    REAL (KIND=DP) :: zeror(3), zeroi(3), errest(3)
    INTEGER :: loc(1), pts, ifail, root
!?!?! f and x are inside record_root; coeffl, coeffr, xc and dcoeff are coefficient of the grand potential in terms of powers of phi_m (defined immediately below); zeror, zeroi, errest, loc, pts, ifail and root are used in NAG routine to solve cubic eqn, what do they do? !!!

!!! 'Renormalised' parameters in terms of different populations and angles !!!
    geff=2.0d0*g*sin(chi)*COS(chi)
    UplusVeff=(2.0d0*U*((COS(chi)**4.0d0)+(SIN(chi)**4.0d0))+4.0d0*V*(SIN(chi)**2.0d0)*(COS(chi)**2.0d0))
    ! yeff goes in place of TWICE y0 without the prefactor of 0.5
    yeff=yu*COS(chi)**2.0d0+yd*SIN(chi)**2.0d0+2.0d0*yud*SIN(chi)*COS(chi)

    ! Set up critical value of psim (where function changes),
    ! and coefficients to left and to right.
    !xc=yu/(g*TAN(chi))+yd*TAN(chi)/g+2.0d0*yud/g
    xc=2.0d0*yeff/geff

    ! Coefficients are coefficients of 1, x, x^2, x^3, x^4; coefficient
    ! of x^3 is always zero
    ! For large psim, the optimal atomic condensate is zero, so
    ! one gets the bare values of coefficients.
    coeffr = (/ 0.0D0, msrc, -2.0d0*ym, 0.0D0, W /)

    ! For small psim, the optimal atomic condensate is non-zero
    ! so one gets modified coefficients.
    coeffl = coeffr - (/ yeff*yeff, -geff*yeff, 0.25d0*geff*geff, 0.0D0, 0.0D0/)/UplusVeff
!!! The new coefficients are in terms of the angle chi !!!


    IF (DBG_Psim_F) THEN
        WRITE(98,'(70("#"))')
        WRITE(98,*) coeffl, coeffr
        WRITE(98,'(70("#"))')
    end IF
!!! Error messages !!!

    ! Zero the counter:
    pts=0

    ! Find extrema for LHS, by solving cubic equation and restricting
    ! to real solutions with x<xc.
    dcoeff=coeffl(2:5)*(/ 1,2,3,4 /); IFAIL=-1
    CALL C02AKF(dcoeff(4), dcoeff(3), dcoeff(2), dcoeff(1), &
            & zeror, zeroi, errest, ifail)
!!! Sets coeffs of differential of quartic eqn, i.e. cubic equation, to find roots !!!

    IF (IFAIL .NE. 0) THEN
        WRITE(*,*) "Coefficients:"
        WRITE(*,*) dcoeff
        WRITE(*,*) yu, yd, yud, ym, msrc
        STOP
    end IF
!!! To keep track if error !!!

    DO root=1,3
        IF (ABS(zeroi(root)) .LT. 5.0D0*errest(root) &
            & .AND. zeror(root) .LT. xc) &
            & CALL record_root(zeror(root), coeffl)
    end DO
!!! sets roots out of interval right !!!

    ! Record mid-point as a possible solution
    CALL record_root(xc, coeffl)

    ! Find extrema for RHS< by cubic with x>xc
    dcoeff=coeffr(2:5)*(/ 1,2,3,4 /); IFAIL=0
    CALL C02AKF(dcoeff(4), dcoeff(3), dcoeff(2), dcoeff(1), &
    & zeror, zeroi, errest, ifail)
    DO root=1,3
        IF (ABS(zeroi(root)) .LT. 5.0D0*errest(root) &
            & .AND. zeror(root) .GT. xc) &
            & CALL record_root(zeror(root), coeffr)
    end DO


IF (DBG_Psim_F) WRITE(98,*)

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Choose which one to use:
    loc=MINLOC(f(1:pts)); psim=x(loc(1))

    ! Find the corresponding psi0sq
    psi0sq=MAX(0.0D0 , (2.0d0*yeff - geff*psim)/(2.0d0*UplusVeff))

    CONTAINS
!!! Subroutine find_optimal_psi contains a subroutine record_root !!!

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! NAME:   record_root
    ! VARIABLES:
    !           xval  - Current point
    !           coeff - Coefficients to calculate f
    ! SYNOPSIS:
    ! Record a point, and increment counters appropriately
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    SUBROUTINE record_root(xval,coeff)
!?!?! Where do xval/coeff come from? !!!
        REAL (KIND=DP), INTENT(IN) ::xval,coeff(5)
        REAL (KIND=DP) :: powers(5)

        pts=pts+1
        x(pts)=xval

        powers = (/ 1.0D0, xval, xval**2, xval**3, xval**4 /)

        f(pts)= SUM(coeff*powers)
!!! This gets the coefficients and puts them back with the right powers of psim !!!

    END SUBROUTINE record_root

END SUBROUTINE find_optimal_psi


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! NAME:   get_mf_f_parts
! VARIABLES:
!           F   - Total free energy
!           psi0sq  - Atomic condensate density
!           psim    - Molecular field
! SYNOPSIS:
! Use the above routines to get the mean field free energy and
! order parameters.  Useful really only as a test of the above routines.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

SUBROUTINE get_mf_f_parts(chi,F,psi0sq,psim)
    USE global, ONLY: U,V,W,g, nu, mu, h
    REAL (KIND=DP), INTENT(IN) :: chi
    REAL (KIND=DP), INTENT(OUT) ::F,psi0sq,psim
    REAL (KIND=DP) :: yu, yd, yud, ym, msrc, muup, mudown
    REAL (KIND=DP) ::geff, UplusVeff, yeff

    muup=mu+h; mudown=mu-h

    ! Set ys to their values when pairing fields vanish
    yu=muup; yd=mudown; yud=0.0D0; ym=mu-0.50D0*nu; msrc=0.0D0

!!! 'Renormalised' parameters in terms of different populations and angles !!!
    geff=2.0d0*g*sin(chi)*COS(chi)
    UplusVeff=(2.0d0*U*((COS(chi)**4.0d0)+(SIN(chi)**4.0d0))+4.0d0*V*(SIN(chi)**2.0d0)*(COS(chi)**2.0d0))
    yeff=yu*COS(chi)**2.0d0+yd*SIN(chi)**2.0d0+2.0d0*yud*SIN(chi)*COS(chi)


    ! Calculate psi0sq and psim
    CALL find_optimal_psi(yu,yd,yud,ym,msrc,chi,psi0sq, psim)

    ! Find the total grand potential
    F = - 2.0d0*yeff*psi0sq + UplusVeff*psi0sq**2.0d0 &
        & +  geff*psi0sq*psim - 2.0d0* ym*psim**2.0d0 + W*psim**4.0d0

END SUBROUTINE get_mf_f_parts


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! NAME:   get_F_parts
    ! VARIABLES:
    !           a,beta,xi,chi - Variational parameters
    !           F0 - Bare grand potential, including all part.  NB
    !                    this is F0 as defined in the notes, including Z
    !           yu,yd,yud, ym - COfficients of psi0sq and psim
    !           msrc   - Pair source for psim
    !           psi0sq - psi_0^2, atomic condensate density
    !           psim   - psim, molecular condensate field
    !           norm   - Total normal density (incl. molecules)
    ! SYNOPSIS:
    ! Calculate the two parts of grand potential required elsewhere
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

SUBROUTINE get_F_parts(p_a, p_beta, xi, chi, F0, yu, yd, yud, ym, msrc, psi0sq, psim, norm)
    USE interpolation, ONLY: get_integral
!!! Get integral uses interpolation if on, or just calculates integral !!!
    USE global, ONLY: U, V, W, g, nu, eX, mu, pi, T, mmass, h

    USE global, ONLY: minbeta, alimit, finite_T_cutoff

    REAL (KIND=DP), INTENT(IN) :: p_a(3), p_beta(3), xi, chi
    REAL (KIND=DP), INTENT(OUT) ::F0, yu, yd, yud, ym, msrc, psi0sq, psim, norm
    REAL (KIND=DP) :: a(3), beta(3)
    REAL (KIND=DP) ::  alpha(3), cutoff(3), muup, mudown, mum
    REAL (KIND=DP) :: n(3), Phi(3), Z(3), root0(3), root1(3)
    Real (KIND=DP) ::  cosqxi, sinsqxi

    beta=p_beta; a=p_a

    muup=mu+h; mudown=mu-h; mum=mu-0.5D0*nu

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Calculate intermediate quantities:
    ! The following should not be required, but for some reason
    ! the nag routine for minimisation insists on calculating
    ! the function oustide the region where it is defined!

    WHERE (beta .LT. minbeta)
        beta = minbeta
    end WHERE

    WHERE (ABS(a) .GT. alimit)
        a=alimit*a/ABS(a)
    end WHERE
!!! .LT.=less than .GT.=greater than !!!

    ! Cutoff different for different channels (same momentum, varying
    ! energy.
    cutoff(1:2)=eX; cutoff(3)=eX/mmass

    alpha=a*beta

    ! Temporary quantities appearing below: mode energies at upper and
    ! lower limits of integral
    root0=sqrt((beta+alpha)*(beta-alpha))
    root1=cutoff*sqrt((1.0D0+(beta+alpha)/cutoff)*(1.0D0+(beta-alpha)/cutoff))

    ! These are the n_a, n_b, Phi_a, Phi_b, Z_a, Z_b terms
    WHERE (beta .EQ. 0.0D0)
        n=0.0D0
        Phi=0.0d0
        Z=0.0D0
    ELSEWHERE
        n=(alpha**2.0d0)*(cutoff+root1-root0)&
            & /((beta+root0)*(cutoff+beta+ root1)*(4.0d0*pi))
        Phi=alpha*log( ( cutoff+beta+root1 )/(beta+root0) )/(4.0d0*pi)
        Z=0.5D0*alpha*Phi-beta*n + &
            & (alpha**4.0d0) * (1.0D0+(root1-root0)/cutoff) &
            &            * (1.0D0+(2*beta+root1+root0)/cutoff) / &
            & (16*pi*((beta+root0)*(1.0D0+(beta+ root1)/cutoff))**2.0d0)
    end WHERE
!?!?! These do not change do they? !!!

    ! No change for zero T case.
    IF (T .GT. 0.0D0) THEN
    ! Finite T part of n is analytic.  Note that upper limit
    ! only contributes if a finite cutoff exists.
        IF (finite_T_cutoff) THEN
            WHERE (root1/T .GT. 10.0D0)
                n=n + T * (-exp(-root1/T) - 0.50d0*exp(-2.0d0*root1/T) )/(2.0d0*pi)
            ELSEWHERE
                n=n + T * Log(1-exp(-root1/T))/(2.0d0*pi)
            end WHERE
        end IF

        WHERE (root0/T .GT. 10.0D0)
            n=n - T * (-exp(-root0/T) - 0.50d0*exp(-2.0d0*root0/T) )/(2.0d0*pi)
        ELSEWHERE
            n=n - T * Log(1-exp(-root0/T))/(2.0d0*pi)
        end WHERE

    ! Add the finite temperature parts to Phi and Z-TS, note the different
    ! lookup function ..B used for the molecules.
        Phi(1) = Phi(1) + get_integral(a(1), beta(1), "FP")
        Phi(2) = Phi(2) + get_integral(a(2), beta(2), "FP")
        Phi(3) = Phi(3) + get_integral(a(3), beta(3), "FB")

        Z(1) = Z(1) + get_integral(a(1), beta(1), "ZP")
        Z(2) = Z(2) + get_integral(a(2), beta(2), "ZP")
        Z(3) = Z(3) + get_integral(a(3), beta(3), "ZB")
    end IF

    ! Rescale 3rd parts:
    n(3) = n(3) * mmass
    Phi(3) = Phi(3) * mmass
    Z(3) = Z(3) * mmass

    !WRITE(*,'(3(2(F8.5,2X),3X))') alpha, beta, Z

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Calculate final quantities.
    ! Note that na + nb = nup + ndown in new notation.
    norm=SUM(n(1:2)) + 2*n(3)

    cosqxi=COS(xi)**2.0d0
    sinsqxi=SIN(xi)**2.0d0


   ! F0=SUM(Z)-muup*(n(1)*COS(xi)**2+n(2)*SIN(xi)**2) &
   !     &    -mudown*(n(2)*COS(xi)**2+n(1)*SIN(xi)**2) &
   !     & - 2*mum*n(3) + 0.5d0*U*(2*(n(1)*COS(xi)**2+n(2)*SIN(xi)**2)**2 &
   !     & + 2*(n(2)*COS(xi)**2+n(1)*SIN(xi)**2)**2+(Phi(1)*COS(xi)**2+ &
   !     & Phi(2)*SIN(xi)**2)**2+(Phi(2)*COS(xi)**2+ &
   !     & Phi(1)*SIN(xi)**2)**2) + V*(n(1)*n(2)*(COS(xi)**2-SIN(xi)**2)**2 &
   !     & +SIN(xi)**2*COS(xi)**2*(Phi(2)-Phi(1))**2 &
   !     & + 2*SIN(xi)**2*COS(xi)**2*(n(1)**2+n(2)**2)) &
   !     & +W*(2*n(3)**2 + Phi(3)**2)

    F0=SUM(Z) -muup * (n(1)*cosqxi+n(2)*sinsqxi) &
        &   -mudown * (n(2)*cosqxi+n(1)*sinsqxi) &
        &   - 2.0d0 * mum * n(3) &
        & + 0.5d0 * U *( 2.0d0 * (n(1) * cosqxi + n(2) * sinsqxi )**2.0d0 &
        &              + 2.0d0 * (n(2) * cosqxi + n(1) * sinsqxi )**2.0d0 &
        &              + ( Phi(1) * cosqxi + Phi(2) * sinsqxi )**2.0d0 &
        &              + ( Phi(2) * cosqxi + Phi(1) * sinsqxi )**2.0d0) &
        & + V * ( n(1) * n(2) * (cosqxi - sinsqxi)**2.0d0 + &
        &         sinsqxi * cosqxi * (Phi(2) - Phi(1))**2 + &
        &         2.0d0 * sinsqxi * cosqxi * (n(1)**2 + n(2)**2.0d0) ) &
        & + W * (2.0d0 * n(3)**2 + Phi(3)**2)

    ! Intermediate quantities for finding psim and psi0sq (atom
    ! condensate density)

    yu =   muup - U * ( 2.0d0 * (n(1) * cosqxi + n(2) * sinsqxi ) &
        &               + (Phi(1) * cosqxi + Phi(2) * sinsqxi)) &
        &       - V * ( n(2) * cosqxi + n(1) * sinsqxi)

    yd = mudown - U * ( 2.0d0 * (n(2) * cosqxi + n(1) * sinsqxi ) &
        &              + (Phi(2) * cosqxi + Phi(1) * sinsqxi) ) &
        &       - V * ( n(1) * cosqxi + n(2) * sinsqxi )

    yud =       - V * SIN(xi) * COS(xi) * ( (n(2)-n(1)) + &
            &                               (Phi(2)-Phi(1)) )

    ym = mum    - W * (2.0d0 * n(3) + Phi(3))

    msrc =  Sin(xi) * COS(xi) * g * (Phi(2) - Phi(1))

    ! Calculate psi0sq and psim
    CALL find_optimal_psi(yu,yd,yud,ym, msrc,chi,psi0sq, psim)

END SUBROUTINE get_F_parts

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! NAME:   calculate_F
! VARIABLES:
!           a, beta - Variational parameters for a,b,m
! SYNOPSIS:
! Calculate the grand potential at the given parameters
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION calculate_F(a,beta,xi,chi)
    USE global, ONLY: U, V, g, W, nu, mu, h

    REAL (KIND=DP), INTENT(IN) ::a(3),beta(3), chi, xi
    REAL (KIND=DP) :: calculate_F
    REAL (KIND=DP) :: F0, psim, psi0sq, yu, yd, yud, ym, msrc, norm
    REAL (KIND=DP)::geff,UplusVeff,yeff


    CALL get_F_parts(a, beta,xi, chi, F0, yu,yd,yud, ym, msrc, psi0sq, psim, norm)

    geff=2.0d0*g*sin(chi)*COS(chi)
    UplusVeff= 2.0d0 * U * ((COS(chi)**4) + (SIN(chi)**4.0d0))&
         &   + 4.0d0 * V *  (SIN(chi)**2) * (COS(chi)**2.0d0)

    yeff=yu*COS(chi)**2+yd*SIN(chi)**2+2*yud*SIN(chi)*COS(chi)

    calculate_F = F0 - 2.0d0*yeff*psi0sq +  UplusVeff*psi0sq**2.0d0 &
                    &  - 2.0d0 * ym*psim**2.0d0 + W*psim**4.0d0 &
                    & + (msrc+geff*psi0sq)*psim


    IF (DBG_F) WRITE(99,'(X,2(3(D15.8,2X),3X),(5(D15.8,2X)))') &
    &  a, beta,xi,chi, calculate_F, F0, psi0sq, psim

END FUNCTION calculate_F


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! NAME:   get_F_....
! VARIABLES:
!           n     - NUmber of variables (6)
!           xc    - Variational parameters
!           fc    - Grand Potential (to return)
!           iuser - Unused
!           ruser - Unused
! SYNOPSIS:
! Calculate the Grand Potential vs various ' parameters.  In the one
! parameter case, a is taken from ruser(1).  In the one, two parameter
! case we use iuser(1) to determine what alpha, beta to set.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE get_F_8D(n,xc,fc,iuser,ruser)
    USE global, ONLY: minbeta

    INTEGER, INTENT(IN) :: n
    REAL (KIND=DP), INTENT(IN) :: xc(n)
    REAL (KIND=DP), INTENT(OUT) :: fc
    INTEGER, INTENT(INOUT) :: iuser(*)
    REAL (KIND=DP), INTENT(INOUT) :: ruser(*)
    REAL (KIND=DP) :: dummy, aa(3), bb(3), chi, xi

    dummy=ruser(1); dummy=1.0D0*iuser(1)
    aa=xc(1:3); bb=xc(4:6); xi=xc(7); chi=xc(8)

    fc=calculate_F(aa,bb, xi, chi)


END SUBROUTINE get_F_8D

SUBROUTINE get_F_6D(n,xc,fc,iuser,ruser)
    USE global, ONLY: minbeta

    INTEGER, INTENT(IN) :: n
    REAL (KIND=DP), INTENT(IN) :: xc(n)
    REAL (KIND=DP), INTENT(OUT) :: fc
    INTEGER, INTENT(INOUT) :: iuser(*)
    REAL (KIND=DP), INTENT(INOUT) :: ruser(*)
    REAL (KIND=DP) :: dummy, aa(3), bb(3), chi, xi

    dummy=ruser(1); dummy=1.0D0*iuser(1)
    aa=xc(1:3); bb=xc(4:6);

    xi=0.25D0*pi; chi=0.25D0*pi

    fc=calculate_F(aa,bb, pi/4, pi/4)

END SUBROUTINE get_F_6D


SUBROUTINE get_F_4D(n,xc,fc,iuser,ruser)
    INTEGER, INTENT(IN) :: n
    REAL (KIND=DP), INTENT(IN) :: xc(n)
    REAL (KIND=DP), INTENT(OUT) :: fc
    INTEGER, INTENT(INOUT) :: iuser(*)
    REAL (KIND=DP), INTENT(INOUT) :: ruser(*)
    REAL (KIND=DP) :: dummy, aa(3), bb(3), chi, xi

    dummy=ruser(1);
    ! Atomic a and beta
    aa(1:2) = afac(1:2, iuser(1))*xc(1)
    bb(1:2) = bfac(1:2, iuser(1))*xc(3)
    ! Molecular terms
    aa(3) = xc(2);  bb(3) = xc(4); xi=xc(7); chi=xc(8)
    fc=calculate_F(aa, bb, pi/4, pi/4)

END SUBROUTINE get_F_4D

SUBROUTINE get_F_NS_3D(n,xc,fc,iuser,ruser)
    INTEGER, INTENT(IN) :: n
    REAL (KIND=DP), INTENT(IN) :: xc(n)
    REAL (KIND=DP), INTENT(OUT) :: fc
    INTEGER, INTENT(INOUT) :: iuser(*)
    REAL (KIND=DP), INTENT(INOUT) :: ruser(*)
    REAL (KIND=DP) :: dummy, aa(3), bb(3), chi, xi

    dummy=ruser(1);  dummy=1.0D0*iuser(1)

    ! Only beta term present
    aa=0.0D0; bb=xc;

    xi=0.25D0*pi; chi=0.25D0*pi
    fc=calculate_F(aa, bb, pi/4, pi/4)

END SUBROUTINE get_F_NS_3D

SUBROUTINE get_F_MSF_4D(n,xc,fc,iuser,ruser)
    INTEGER, INTENT(IN) :: n
    REAL (KIND=DP), INTENT(IN) :: xc(n)
    REAL (KIND=DP), INTENT(OUT) :: fc
    INTEGER, INTENT(INOUT) :: iuser(*)
    REAL (KIND=DP), INTENT(INOUT) :: ruser(*)
    REAL (KIND=DP) :: dummy, aa(3), bb(3), chi, xi

!PRINT *, "I'm using get_f_msf_4d now"
!!! What do dummies do exactly ?
    dummy=ruser(1); dummy=1.0D0*iuser(1)

    ! aa_a=aa_b=0 (no atomic coherence at all)
    aa(1)=0.0d0; aa(2)=0.0d0

    ! Variational terms are aa m, beta a, beta b, beta m
    aa(3) = xc(1); bb(1)=xc(2); bb(2)=xc(3); bb(3) = xc(4)

    xi=0.25D0*pi; chi=0.25D0*pi

    fc=calculate_F(aa,bb,pi/4, pi/4)

END SUBROUTINE get_F_MSF_4D

END MODULE free_energies





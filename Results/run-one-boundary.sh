#!/bin/bash


do_combine=0

# Physical parameters
. params.GaAs
# Ranges etc
. common-params
. vs-detun-params
#. vs-T-params
T=0.340D0


h=0.220d0
prefix="boundary_${material}_T=${T}_h=${h}"

run_one() {


    cat <<EOF > CONTROL.single
$U0, $V0, $W0, $mratio, $mex, $h
$Delta0, $Rabi, $ebind_x, $ebind_xx, $delta_exph, $mu, $T
$allow_sigma
$na_int, $nb_int, $maxb_int
$amin, $amax, $Na, $a
$bmin, $bmax, $Nbgrid, $Nbiter, $b
$prefix, $mft, 0, $interpolate
EOF


    ../ex_ph_boundary < CONTROL.single 
}

######################################################################
# Main programme starts here
######################################################################


amin=20.0d0
amax=40.0d0
amx=1
Na=41

interpolate="N"

#prefix="test-${prefix}"
run_one





HOSTNAME := $(shell hostname)

a2ps=a2pscolor


include Makefile.$(HOSTNAME)


#DEBUG=-O3 -fopenmp
DEBUG=-fbounds-check

WARN=-Wall 

LDFLAGS= $(DEBUG) $(NAGDIR)  $(LAPACKDIR) $(ARCHLDFLAGS)
FFLAGS= $(NAGINC)  $(DEBUG) $(WARN) $(ARCHFFLAGS)
LOADLIBES=$(NAGLIB)

FC=gfortran
LD=gfortran

PROGRAMS=ex_ph_phase ex_ph_boundary widbg

all: $(PROGRAMS)
	@echo "DONE"

TEMPLATE=
TEMPLATE: $(TEMPLATE)
	$(LD) $(LDFLAGS) $(TEMPLATE) -o $@ $(LOADLIBES) 

fixed_eb=utility.o global.o interpolation.o free_energies.o minimisation.o fixed_eb_phase.o
fixed_eb: $(fixed_eb)
	$(LD) $(LDFLAGS) $(fixed_eb) -o $@ $(LOADLIBES) 

ex_ph_phase=utility.o global.o interpolation.o free_energies.o minimisation.o ex_ph_params.o ex_ph_phase.o
ex_ph_phase: $(ex_ph_phase)
	$(LD) $(LDFLAGS) $(ex_ph_phase) -o $@ $(LOADLIBES) 

ex_ph_boundary=utility.o global.o interpolation.o free_energies.o minimisation.o ex_ph_params.o ex_ph_boundary.o
ex_ph_boundary: $(ex_ph_boundary)
	$(LD) $(LDFLAGS) $(ex_ph_boundary) -o $@ $(LOADLIBES) 


widbg=utility.o global.o interpolation.o widbg_free_energy.o widbg_minimisation.o widbg.o
widbg: $(widbg)
	$(LD) $(LDFLAGS) $(widbg) -o $@ $(LOADLIBES) 

print: 
	$(a2ps) $(patsubst %.o, %.f90, $(ex_ph_phase))


%.o: %.f90
	$(FC) $(FFLAGS) -c $*.f90

%.mod: %.o

clean:
	rm -f *.o *.mod $(PROGRAMS)

realclean: clean
	rm -f *~

MODULE minimisation
  USE global, ONLY: DBG_F

  IMPLICIT NONE
  INTEGER,PRIVATE, PARAMETER :: DP=KIND(1.0D0)

  LOGICAL :: DBG_ATTEMPTS=.FALSE.
  INTEGER :: DBG_ATTEMPT_FILE=200

  ! This module contains the code for minimisation; both the minimisation
  ! of the separate grand potential forms, and the comparison between
  ! the two free energies
  
CONTAINS
  
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   find_minimum
  ! VARIABLES:
  !           a, beta, xi, chi - Parameters of variational wavefunction
  !           F       - Grand Potential of minimum found
  !           psi0sq  - Atomic condensate density
  !           psim    - Molecular condensate
  !           rho     - Density
  ! SYNOPSIS:
  ! Find the mininimum grand potential state for the current parameters
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE find_minimum(a, beta, xi, chi, F, psi0sq, psim,rho)
    USE free_energies, ONLY: get_f_8d, get_f_6d, get_f_ns_3d, get_f_msf_4d, &
         &  afac, bfac, calculate_F, get_f_parts
    USE global, ONLY: allow_sigma, alimit, minbeta, pi
    USE utility, ONLY: interpolate

    REAL (KIND=DP), INTENT(OUT) ::a(3), beta(3), xi, chi, F,psi0sq, psim, rho
    ! Maximum for range of beta
    REAL (KIND=DP) :: maxbeta=1.0D2
    ! Upper value for starting guesses of beta
    REAL (KIND=DP) :: maxbeta0=3.0D0
    INTEGER, PARAMETER :: Na=11, Nb=11, attempts=80001
    
    REAL (KIND=DP) :: F0, norm,yu,yd,yud, ym, msrc

    LOGICAL :: acceptable
    INTEGER :: ia, ib,iangle, attempt, mode, maxmode, iuser(1), i_min(1)
    REAL (KIND=DP) :: a_trial, beta_trial, ruser(2), x(8)
    REAL (KIND=DP) :: f_m(attempts), x_m(attempts,8), aa(3), bb(3)

    LOGICAL :: DO_8D=.TRUE., DO_6D=.FALSE., DO_3D=.FALSE., DO_MSF=.FALSE.


    IF (DBG_F) WRITE(99,'("#",2(3(A15,2X),3X),(5(A15,2X)))') &
         &  "a","a","a", "beta","beta","beta", "F", "F0", "psi0sq", "psim"

    f_m=0.0D0; x_m=0.0D0

    attempt=0
    
       ! Allow varying alpha;  choose all possible modes
       maxmode=4;


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Vacuum state:
    IF (DBG_ATTEMPTS) WRITE(DBG_ATTEMPT_FILE,'(A8)',ADVANCE='NO') "Vacuum"
    aa=0.0D0; bb=maxbeta;
    CALL record_attempt(aa, bb, pi/4, pi/4)


    DO mode=1,maxmode

       DO ib=1, Nb
          beta_trial =INTERPOLATE(0.1D0, maxbeta0, ib, Nb,.TRUE.)

          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          ! Six or eight dimensional minimisation
          DO ia=1, Na
             a_trial=INTERPOLATE(-alimit**2, alimit**2, ia, Na,.FALSE.)
             
                !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                ! Eight dimensional (i.e. all alpha, beta, xi, chi varying)
                IF (DO_8D) THEN

                DO iangle=1,17

                   x(7) = (iangle-1)*pi/16.0d0
                   x(8) = (iangle-1)*pi/16.0d0
               
                !  WRITE(*,*) "Running 8D minimisation"
                ! Set the initial point for this attempt
                   x(1:3) = a_trial*afac(:,mode)
                   x(4:6) = beta_trial*bfac(:,mode)
        

                   CALL nag_wrapper(8, "8D", get_f_8d, iuser, ruser, x, &
                        & (/-alimit,-alimit,-alimit, minbeta,minbeta,minbeta, -pi, -pi /), &
                        & (/ alimit, alimit, alimit, maxbeta,maxbeta,maxbeta, pi, pi /), &
                        & f, acceptable)

                   IF (acceptable) CALL record_attempt(x(1:3),x(4:6),x(7),x(8))
                   END DO
                end IF

             !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
             ! Six dimensional (i.e. all alpha, beta varying)
             IF (DO_6D) THEN
                 !  WRITE(*,*) "Running 6D minimisation"
                ! Set the initial point for this attempt
                x(1:3) = a_trial*afac(:,mode)
                x(4:6) = beta_trial*bfac(:,mode)

                CALL nag_wrapper(6, "6D", get_f_6d, iuser, ruser, x, &
                     & (/-alimit,-alimit,-alimit, minbeta,minbeta,minbeta /), &
                     & (/ alimit, alimit, alimit, maxbeta,maxbeta,maxbeta /), &
                     & f, acceptable)


                IF (acceptable) CALL record_attempt(x(1:3),x(4:6),pi/4, pi/4)
             end IF
             

          end DO
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          ! Three dimensional (i.e. normal state)
          IF (DO_3D) THEN
             ! Set the initial point for this attempt
             x(1:3) = beta_trial*bfac(:,mode)
             
             CALL nag_wrapper(3, "NS 3D", get_f_ns_3d, iuser, ruser, x, &
                  & (/ minbeta, minbeta, minbeta /), &
                  & (/ maxbeta, maxbeta, maxbeta /), &
                  & f, acceptable)
             
             aa=0.0D0; bb=x(1:3)
             IF (acceptable) CALL record_attempt(aa,bb, pi/4, pi/4)
          end IF

          ! Four-dimensional zero atomic field:
          IF (DO_MSF) THEN
             
             x(1) = a_trial
             !???! What is the right form for the surviving a (a_m)?
             x(2:4) = beta_trial*bfac(:,mode)
             
             CALL nag_wrapper(4, "MSF 4D", get_f_msf_4d, iuser, ruser, x, &
                  & (/-alimit, minbeta, minbeta, minbeta /), &
                  & (/alimit, maxbeta, maxbeta, maxbeta /), &
                  & f, acceptable)
             
             aa(1:2)=0.0D0; aa(3)=x(1); bb=x(2:4);

             IF (acceptable) CALL record_attempt(aa,bb, pi/4, pi/4)
          end IF
          
       end DO
    end DO

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Determine which of the various attempts was overall best
    i_min=MINLOC(f_m(1:attempt))   
    f=f_m(i_min(1)); a=x_m(i_min(1),1:3); beta=x_m(i_min(1),4:6)
    xi=x_m(i_min(1),7); chi=x_m(i_min(1),8)

    ! Find psi0sq, psim, and make sure it is positive
    CALL get_f_parts(a,beta, xi, chi, F0, yu, yd, yud, ym, msrc, psi0sq, psim, norm)
    psi0sq=MAX(psi0sq, 0.0D0)

    ! Total density: note that each molecule contributes 2 to density.
    rho = 2.0d0*psi0sq  + 2.0d0*psim**2 + norm
    
    IF (DBG_ATTEMPTS) THEN 
       WRITE(DBG_ATTEMPT_FILE,*) "Choose attempt", i_min
       WRITE(*,*) "Choose attempt", i_min
       WRITE(DBG_ATTEMPT_FILE,*)
    end IF
    
    
  CONTAINS

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! NAME:   record_attempt
    ! SYNOPSIS:
    ! Record the current values of a, beta in the list of possible
    ! minima. (Find out the value of F at this point, and record
    ! it too)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    SUBROUTINE record_attempt(a,beta, xi, chi)
      REAL (KIND=DP), INTENT(IN) :: a(3), beta(3), xi, chi

      ! Increment counter
      attempt=attempt+1
      IF (attempt .GT. SIZE(f_m)) THEN
         WRITE(*,*) "Overran attempt record"
         STOP
      end IF

      f_m(attempt)=calculate_f(a,beta,xi,chi)
      IF(DBG_F) WRITE(99,*)

      x_m(attempt,1:3) = a
      x_m(attempt,4:6) = beta
      x_m(attempt,7)= xi
      x_m(attempt, 8)= chi

      IF (DBG_ATTEMPTS) WRITE(DBG_ATTEMPT_FILE,'(I5,2X,3(3(D12.5,X),2X))')&
           &  attempt, x_m(attempt,:), f_m(attempt)

    END SUBROUTINE record_attempt

  END SUBROUTINE find_minimum


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   nag_wrapper
  ! VARIABLES:
  !           dim
  !           descriptor
  !           routine
  !           x
  !           bl, bu
  !           f
  ! SYNOPSIS:
  !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE nag_wrapper(dim, descriptor, routine, iuser, ruser, &
       & x, bl0, bu0, f, acceptable)
    USE nag_library, ONLY: e04jyf, e04aba

    INTEGER, INTENT(IN) :: dim
    CHARACTER(LEN=*), INTENT(IN) :: descriptor
    REAL (KIND=DP), INTENT(INOUT) ::x(dim)
    REAL (KIND=DP), INTENT(IN) :: bl0(dim),bu0(dim)
    INTEGER, INTENT(INOUT) :: iuser(*)
    REAL (KIND(1.0D0)), INTENT(INOUT) :: ruser(*)
    REAL (KIND=DP), INTENT(OUT) :: f   
    LOGICAL, INTENT(OUT) :: acceptable

    INTERFACE
       SUBROUTINE routine(n,xc,fc,iuser,ruser)
         INTEGER, INTENT(IN) :: n
         REAL (KIND(1.0D0)), INTENT(IN) :: xc(n)
         REAL (KIND(1.0D0)), INTENT(OUT) :: fc
         INTEGER, INTENT(INOUT) :: iuser(*)
         REAL (KIND(1.0D0)), INTENT(INOUT) :: ruser(*)
       end SUBROUTINE routine
    end INTERFACE

    ! Lengths of work arrays required for E04JYF
    INTEGER, PARAMETER :: liw=10
    INTEGER, PARAMETER :: lw=150

    INTEGER ::  iw(liw), ifail, ibound
    REAL (KIND=DP) :: w(lw), bl(dim), bu(dim)
    
    IF (DBG_ATTEMPTS) WRITE(DBG_ATTEMPT_FILE,'(A8)',ADVANCE='NO') descriptor
    
    bl=bl0; bu=bu0 ! Required because NAG overwrites these
    ibound=0
    ifail= +1

    CALL e04jyf(dim, ibound, routine, bl, bu, x, &
         & f, iw, liw, w, lw, iuser, ruser, ifail)

    CALL check_error(ifail,acceptable)
    IF (acceptable) THEN
       IF(DBG_F) WRITE(99,*) "# Completed ", descriptor
    ELSE
       IF(DBG_F) WRITE(99,*) "# Failed 8D"
       IF(DBG_ATTEMPTS) WRITE(DBG_ATTEMPT_FILE,*) "Fail"
    end IF


  CONTAINS

    SUBROUTINE check_error(ifail, acceptable)
      INTEGER :: ifail
      LOGICAL :: acceptable

      SELECT CASE(IFAIL)
      CASE(1)
         WRITE(*,*) "Invalid input to minimum finding routine"
         acceptable=.FALSE.
         STOP
      CASE(2)
         acceptable=.FALSE.
      CASE DEFAULT
         acceptable=.TRUE.
      end SELECT

    end SUBROUTINE check_error



  END SUBROUTINE nag_wrapper


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   get_soln_type
  ! VARIABLES:
  !           a, beta, psi0sq, psim - Variational params
  ! SYNOPSIS:
  ! Determine what type of solution this is
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION get_soln_type(a,beta,psi0sq,psim) result(typev)
    REAL (KIND=DP), INTENT(IN) ::a(3),beta(3),psi0sq,psim
    INTEGER :: typev

    REAL (KIND=DP) :: alpha(3)
    REAL (KIND=DP), PARAMETER :: psim_cutoff=1.0D-3, alpha_cutoff=1.0D-3

    alpha=a*beta
    IF ((ABS(psim) .LT. psim_cutoff) .AND. (psi0sq .GT. 0.0D0)) THEN
        typev=3 ! ASF
    ELSE IF (psi0sq .GT. 0.0D0) THEN
       typev=2 ! AMSF
    ELSE IF ((ABS(psim) .GT. psim_cutoff) .OR. &
         &   (COUNT(alpha(1:2) .GT. alpha_cutoff) .GT. 0)) THEN
       typev=1 ! MSF
    else
       typev=0 ! N
    end IF
    


  END FUNCTION get_soln_type


END MODULE minimisation


MODULE interpolation
  IMPLICIT NONE
  INTEGER,PRIVATE, PARAMETER :: DP=KIND(1.0D0)

  ! This module deals with the interpolation of the non-analytic
  ! finite temperature contributions ot the free energy, i.e.
  ! that from Z-TS, and that from Phi.  We store four functions,
  ! i.e. these two functions for both open and closed channel
  ! (polaritons and biexcitons)


  ! Storage of interpolation for the four functions
  REAL (KIND=DP), TARGET, ALLOCATABLE, DIMENSION(:) :: &
       & ZP_LA, ZP_MU, ZP_C, &
       & FP_LA, FP_MU, FP_C, &
       & ZB_LA, ZB_MU, ZB_C, &
       & FB_LA, FB_MU, FB_C

  INTEGER :: ZP_PX, ZP_PY, FP_PX, FP_PY, ZB_PX, ZB_PY, FB_PX, FB_PY, &
       & MaxKnots
  
  REAL (KIND=DP) :: max_beta

  ! Debugging
  LOGICAL :: DBG_INT=.FALSE., DBG=.TRUE.
  INTEGER :: DBG_INT_FILE=99
  LOGICAL :: USE_INTERPOLATION=.TRUE.

  ! Module level control of integrals for functions
  REAL (KIND=DP) :: m_alpha, m_beta
  REAL (KIND=DP) :: rescale_integrand
  CHARACTER :: m_intstr*1

  PRIVATE :: lookup_integral, evaluate_integral
  
CONTAINS
  

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   setup_interpolation
  ! VARIABLES:
  !           p_max_beta
  ! SYNOPSIS:
  ! Set up interpolation based on current temperature and current
  ! value of upper cutoff
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE setup_interpolation(p_max_beta, nbeta, na)
    USE global, ONLY: T, mmass, eX

    REAL (KIND=DP), INTENT(IN) ::p_max_beta
    INTEGER, INTENT(IN) :: nbeta, na

    IF (.NOT. USE_INTERPOLATION) THEN
       WRITE(*,*) "Not setting up interpolation as interpolation switched off"
       RETURN
    end IF
    
 
    ! Record max_beta in module
    max_beta=p_max_beta


    ! If necessary, clear module storage
    IF (ALLOCATED(ZP_LA)) THEN
       DEALLOCATE( ZP_LA, ZP_MU, ZP_C,  FP_LA, FP_MU, FP_C, &
            &      ZB_LA, ZB_MU, ZB_C,  FB_LA, FB_MU, FB_C)
    end IF

    ! NB "Z" means Z-ST, "F" means "Phi"
    ! and P/B means polariton/bipolariton versions

    ALLOCATE(ZP_LA(na+4),    FP_LA(na+4),    ZB_LA(na+4),    FB_LA(na+4))
    ALLOCATE(ZP_MU(2*nbeta+4), FP_MU(2*nbeta+4), ZB_MU(2*nbeta+4), FB_MU(2*nbeta+4))
    ALLOCATE(ZP_C(na*2*nbeta), FP_C(na*2*nbeta), ZB_C(na*2*nbeta), FB_C(na*2*nbeta))

    ! For zero temperature, all these contributions vanish, so don't
    ! calculate.
    IF (T .EQ. 0.0D0)  RETURN

    CALL setup_one_interpolation(ZP_LA, ZP_MU, ZP_C, ZP_PX, ZP_PY, &
         & na, nbeta,  "ZP")
    CALL setup_one_interpolation(FP_LA, FP_MU, FP_C, FP_PX, FP_PY, &
         & na, nbeta,  "FP")
    CALL setup_one_interpolation(ZB_LA, ZB_MU, ZB_C, ZB_PX, ZB_PY, &
         & na, nbeta,  "ZB")
    CALL setup_one_interpolation(FB_LA, FB_MU, FB_C, FB_PX, FB_PY, &
         & na, nbeta,  "FB")

    IF (DBG) WRITE(*,*) "Interpolation setup complete"

    ! Largest number of knots required
    MaxKnots=MAX(ZP_PX, ZP_PY, FP_PX, FP_PY, ZB_PX, ZB_PY, FB_PX, FB_PY)
    
  END SUBROUTINE setup_interpolation
  
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! Variant using only polariton results
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE setup_interpolation_widbg(p_max_beta, nbeta, na)
    USE global, ONLY: T, mmass, eX

    REAL (KIND=DP), INTENT(IN) ::p_max_beta
    INTEGER, INTENT(IN) :: nbeta, na
 
    ! Record max_beta in module
    max_beta=p_max_beta


    ! If necessary, clear module storage
    IF (ALLOCATED(ZP_LA)) THEN
       DEALLOCATE( ZP_LA, ZP_MU, ZP_C,  FP_LA, FP_MU, FP_C)
    end IF

    ! NB "Z" means Z-ST, "F" means "Phi"
    ! and P/B means polariton/bipolariton versions

    ALLOCATE(ZP_LA(na+4),    FP_LA(na+4))
    ALLOCATE(ZP_MU(2*nbeta+4), FP_MU(2*nbeta+4))
    ALLOCATE(ZP_C(na*2*nbeta), FP_C(na*2*nbeta))

    ! For zero temperature, all these contributions vanish, so don't
    ! calculate.
    IF (T .EQ. 0.0D0)  RETURN

    CALL setup_one_interpolation(ZP_LA, ZP_MU, ZP_C, ZP_PX, ZP_PY, &
         & na, nbeta,  "ZP")
    CALL setup_one_interpolation(FP_LA, FP_MU, FP_C, FP_PX, FP_PY, &
         & na, nbeta,  "FP")

    IF (DBG) WRITE(*,*) "Interpolation setup complete"


    ! Largest number of knots required
    MaxKnots=MAX(ZP_PX, ZP_PY, FP_PX, FP_PY)
    
  END SUBROUTINE setup_interpolation_widbg

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   setup_one_interpolation
  ! VARIABLES:
  !           LA, MU, C - Parameters to set
  !           Px, Py    - Number of knots
  !           na, nb    - Ranges to set
  ! SYNOPSIS:
  ! Set up a single interpolating function.  NB, these are not the actual
  ! values of Phi, Z for the 3rd part, but miss a prefactor of mass, which
  ! is added on when considering the finite T part.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE setup_one_interpolation(LA,MU,C,Px,Py,na,nbeta, fstr)
    !USE nag_f77_e_chapter, ONLY: E01DAF
    !USE nag_f77_d_chapter, ONLY: D01AHF
    USE nag_library, ONLY: E01DAF, D01AHF

    USE utility, ONLY: Interpolate
    USE global, ONLY: eX, mmass,T, alimit, finite_T_cutoff

    REAL (KIND=DP), INTENT(OUT) ::LA(:),MU(:),C(:)
    INTEGER, INTENT(OUT) ::Px,Py
    INTEGER, INTENT(IN) ::na,nbeta
    CHARACTER, INTENT(IN) :: fstr*2

    REAL (KIND=DP), ALLOCATABLE :: f(:), a(:), beta(:), WRK(:)
    REAL (KIND=DP) :: zc, ecutoff,  fval
    REAL (KIND=DP) :: epsrel, relerr, mid_beta
    INTEGER :: ia, ibeta, ifail, nintpts, nintmax
    LOGICAL :: gotnan, gotinf
    CHARACTER ::  erstr*3


    ALLOCATE(f(na*2*nbeta),a(na), beta(2*nbeta), WRK((na+6)*(2*nbeta+6)))

    IF (DBG) WRITE(*,*) "Set up interpolation for ", fstr

    ! Choose value to distinguish high/low sampling rate switch
    MID_beta=MIN(0.5*max_beta, MAX(T, 0.1D0))


    ! Decide which function to calculate depending on fstr
    SELECT CASE(fstr)
    CASE("ZP")
       m_intstr="Z"; ecutoff=eX
    CASE("FP")
       m_intstr="F"; ecutoff=eX
    CASE("ZB")
       m_intstr="Z"; ecutoff=eX/mmass
    CASE("FB")
       m_intstr="F"; ecutoff=eX/mmass
    CASE DEFAULT
       WRITE(*,*) "Invalid function type in setup interpolation"
       STOP
    end SELECT
    ! Avoid infinitessimal parts by checking integrand at 0
    ! (it only ever gets smaller). Integral on logarithmic
    ! scale, i.e. z = exp(-e/T) used to find e
    ! If we use a cutoff at finite T:

    IF (finite_T_cutoff) THEN
       zc = exp(-ecutoff/T);  
    ELSE
       zc=0.0D0
    end IF
    

    DO ibeta=1, 2*nbeta
       ! Two part beta scale, 
       IF (ibeta .LE. nbeta) THEN
          beta(ibeta)=INTERPOLATE(0.0D0, mid_beta, ibeta, nbeta+1, .FALSE.)
       ELSE
          beta(ibeta)=INTERPOLATE(mid_beta, max_beta, ibeta-nbeta, nbeta, .FALSE.)
       end IF
       
       m_beta=beta(ibeta)

       DO ia=1, na
          a(ia) = INTERPOLATE(-SQRT(alimit), SQRT(alimit), ia, na, .FALSE.)
          m_alpha=a(ia)*m_beta
          

          ! Attempt to remove infinitessimal factors. If scale is zero,
          ! avoid this rescaling to get 1/inf.
          rescale_integrand = exp(-sqrt(m_beta**2 - m_alpha**2)/T)
          IF (rescale_integrand .EQ. 0.0D0)  rescale_integrand=1.0D0

          IF (DBG_INT) CLOSE(DBG_INT_FILE)
          ifail=1
          epsrel=5.0E-7; nintmax=250000
          fval= D01AHF(zc, 1.0D0, epsrel, nintpts, relerr, &
               &        integrand, nintmax, ifail)
          

          ! NB, rescale by exponential factor to improve fitting
          ! quality.
          f(2*nbeta*(ia-1)+ibeta)=fval
          
          gotnan=ISNAN(fval); gotinf=ISNAN(0.0D0*fval)

          ! Test for invalid numbers, and repeat integral that gave them.
          IF (gotnan .OR. gotinf .OR. IFAIL .NE. 0) THEN
             IF (gotinf) erstr="INF"; IF (gotnan) erstr="NAN"
             IF (IFAIL .NE. 0) WRITE(ERSTR,'(X,I2)') IFAIL

             WRITE(*,*) "Erstr, got ",erstr,"  at ", fstr, a(ia), beta(ibeta)

             WRITE(*,*) "Got: ",fval
             WRITE(*,*) "Limits:", integrand(zc), integrand(1.0D0)

             DBG_INT=.TRUE.; IFAIL=0
             fval= D01AHF(zc, 1.0D0, epsrel, nintpts, relerr, &
                  &        integrand, nintmax, ifail)

             STOP

          end IF
          
       end DO
    end DO

    ! CAllsetup routine:
    IFAIL=0
    CALL E01DAF(na, 2*nbeta, a, beta, f, PX, PY, LA, MU, C, WRK, IFAIL)

    DEALLOCATE(f,a,beta, WRK)
    
  END SUBROUTINE setup_one_interpolation


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   integrand
  ! VARIABLES:
  !           en - Energy
  ! SYNOPSIS:
  ! Integrand for above interpolation functions
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION integrand(z)
    USE utility, ONLY: pi
    USE global, ONLY: T

    REAL (KIND=DP), INTENT(IN) ::z
    REAL (KIND=DP) ::en, integrand, splitting, bose

    en = - T*log(z) ! Express E in terms of z
    splitting=sqrt((m_beta + en - m_alpha)*(m_beta + en + m_alpha))
    bose = 1.0D0/(exp(splitting/T) - 1.0D0)

    SELECT CASE(m_intstr)
    CASE("F")
       ! Phi integral
       integrand = m_alpha*bose/splitting
    CASE("Z")
       ! Z-ST integral
       IF (splitting .GT. 1.0D0) THEN
          integrand = bose  * (m_alpha**2 - m_beta*(en+m_beta))/splitting &
               &    + T * log(1-exp(-splitting/T))
       ELSE
          integrand =  en * (en + m_beta) * bose / splitting &
               & + T * (bose*log(bose) - (bose+1.0D0)*log(bose+1.0D0))
       end IF
    end SELECT

    ! Common measure factor, including rescaling
    integrand = integrand * T/(z*2.0D0*pi)
    
    IF (DBG_INT) WRITE(DBG_INT_FILE,'(4(E12.4E3, X))') z, en, integrand, &
         & rescale_integrand

    integrand = integrand / rescale_integrand
    
  END FUNCTION integrand


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   get_integral
  ! VARIABLES:
  !           a, beta - Parameters for where to lookup
  !           fstr    - Switch controlling which function to consider
  ! SYNOPSIS:
  ! This is a wrapper that either uses the interpolation if interpolation
  ! is switched on, or directly calculates the integral if off.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION get_integral(a,beta,fstr)
    REAL (KIND=DP), INTENT(IN) ::a,beta
    CHARACTER, INTENT(IN) ::fstr*2
    REAL (KIND=DP) :: get_integral
    
    IF (use_interpolation) THEN
       get_integral=lookup_integral(a,beta,fstr)
    ELSE
       get_integral=evaluate_integral(a,beta,fstr)
    end IF

  end FUNCTION get_integral
  
  


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   lookup_integral
  ! VARIABLES:
  !           a, beta - Parameters for where to lookup
  !           fstr    - Switch controlling which function to consider
  ! SYNOPSIS:
  ! Look up the appropriate function.  Proceeds by creating pointer
  ! to interpolation parameters of a given function, and then calling
  ! lookup on that function.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION lookup_integral(a,beta,fstr)
    !USE nag_f77_e_chapter, ONLY: E02DEF
    USE nag_library, ONLY: E02DEF
    USE global, ONLY: T, alimit

    REAL (KIND=DP), INTENT(IN) ::a,beta
    CHARACTER, INTENT(IN) ::fstr*2
    REAL (KIND=DP) ::lookup_integral

    REAL (KIND=DP) :: temp(1), WRK(MaxKnots)
    INTEGER :: IFAIL, IWRK(MaxKnots), PX, PY
    REAL (KIND=DP), POINTER :: LA(:), MU(:), C(:)
    

    SELECT CASE(fstr)
    CASE("ZP")
       LA => ZP_LA; MU => ZP_MU; C => ZP_C; PX=ZP_PX; PY=ZP_PY
    CASE("FP")
       LA => FP_LA; MU => FP_MU; C => FP_C; PX=FP_PX; PY=FP_PY
    CASE("ZB")
       LA => ZB_LA; MU => ZB_MU; C => ZB_C; PX=ZB_PX; PY=ZB_PY
    CASE("FB")
       LA => FB_LA; MU => FB_MU; C => FB_C; PX=FB_PX; PY=FB_PY
    CASE DEFAULT
       WRITE(*,*) "Invalid function type in lookup"
       STOP
    end SELECT

    IF (ABS(a) .GT. SQRT(alimit)) THEN
       WRITE(*,*) "Lookup called for a outside allowed range"
    end IF

    ! Note that zero temperature case requires no lookup (and nothing
    ! was properley calculted)
    IF ((T .GT. 0.0D0) .AND. (beta .LT. max_beta)) THEN
       CALL E02DEF(1, PX, PY, (/ a /), (/ beta /), &
            & LA, MU, C, temp, WRK, IWRK, IFAIL)
       lookup_integral=temp(1) * exp(-beta*sqrt(1-a*a)/T)
    ELSE
       ! For large beta, the result should in fact vanish (if beta
       ! is much larger than temperature, except that something weird
       ! happens at a=1.  FIXME, should handle this better.
       lookup_integral=0.0D0
    end IF
    
  END FUNCTION lookup_integral


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   evaluate_integral
  ! VARIABLES:
  !           a, beta - Parameters for where to lookup
  !           fstr    - Switch controlling which function to consider
  ! SYNOPSIS:
  ! Perform the integral for the given function, 
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION evaluate_integral(a,beta,fstr)
    !USE nag_f77_D_chapter, ONLY: D01AHF
    USE nag_library, ONLY: D01AHF
    USE global, ONLY: mmass, eX, alimit, T

    REAL (KIND=DP) :: evaluate_integral
    REAL (KIND=DP), INTENT(IN) ::a,beta
    CHARACTER, INTENT(IN) ::fstr*2

    REAL (KIND=DP) :: epsrel, relerr,  zc, ecutoff
    INTEGER :: ifail, nintpts, nintmax

    ! Set module level variables
    m_alpha=a*beta
    m_beta=beta
    ! Which integral to evaluate, and what should cutoff be, depending
    ! on whether we're talking about polariton or bipolariton
    SELECT CASE(fstr)
    CASE("ZP")
       m_intstr="Z"; ecutoff=eX
    CASE("FP")
       m_intstr="F"; ecutoff=eX
    CASE("ZB")
       m_intstr="Z"; ecutoff=eX/mmass
    CASE("FB")
       m_intstr="F"; ecutoff=eX/mmass
    CASE DEFAULT
       WRITE(*,*) "Invalid function type in test"
       STOP
    end SELECT

    epsrel=1.0D-4; nintmax=10000

    ! Cutoffs
    zc = exp(-ecutoff/T); rescale_integrand=1.0D0

    ! Do the evaluation
    evaluate_integral &
         & = D01AHF(zc,  1.0D0, epsrel, nintpts, relerr, &
         &          integrand, nintmax, ifail)          

    IF(ABS(evaluate_integral) .GT. 1.0D100) THEN
       evaluate_integral=sign(1.0D100,evaluate_integral)
    end IF
    

  end FUNCTION evaluate_integral
  


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! NAME:   lookup_integral
  ! VARIABLES:
  !           alpha
  !           beta
  !           fstr
  ! SYNOPSIS:
  ! Compare lookup to actual case
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE test_interpolation(fstr, testmax)
    USE utility, ONLY: interpolate
    USE global, ONLY:  alimit

    CHARACTER, INTENT(IN) ::fstr*2
    REAL (KIND=DP), INTENT(IN) :: testmax
    REAL (KIND=DP) :: exact,lookup
    REAL (KIND=DP) :: a, beta
    INTEGER ::  ibeta, nbeta, ia, na
    INTEGER :: TESTFILE=98

    IF (DBG)  WRITE(*,*) "Testing ", fstr

    na=50; nbeta=50
    
    DO ibeta=1, nbeta
       beta=INTERPOLATE(0.0D0, testmax, ibeta, nbeta, .FALSE.)
       
       DO ia=1, na
          a = INTERPOLATE(-alimit, alimit, ia, na, .FALSE.)

          lookup=lookup_integral(a, beta, fstr)
          exact=evaluate_integral(a, beta, fstr)

          WRITE(TESTFILE,*) a, m_beta, lookup, exact

       end DO
       WRITE(TESTFILE,*)
    end DO

    IF (DBG)  WRITE(*,*) "Test complete in fort.", TESTFILE

    
  end SUBROUTINE test_interpolation

END MODULE interpolation

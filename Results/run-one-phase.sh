#!/bin/bash


do_combine=0

# Physical parameters
. params.GaAs
# Ranges etc
. common-params
. vs-detun-params
#. vs-T-params
T=0.0D0


h=0.22
prefix="phase_${material}_T=${T}_h=${h}"


run_one() {


    cat <<EOF > CONTROL.single
$U0, $V0, $W0, $mratio, $mex, $h
$Delta0, $Rabi, $ebind_x, $ebind_xx, $delta_exph, $mu, $T
$allow_sigma
$na_int, $nb_int, $maxb_int
$amin, $amax, $Na, $a
$bmin, $bmax, $Nb, $b
$prefix, 0, $interpolate
EOF


    ../ex_ph_phase < CONTROL.single 
}

######################################################################
# Main programme starts here
######################################################################


amin=9.750d0
amax=9.750d0
Na=1

bmin=-0.460d0
bmax=-0.420d0
Nb=100


interpolate="N"

run_one



